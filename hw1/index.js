//Теоретический вопрос

//Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

//благодаря ему мы можем создать функции, чтобы их не дублировать


class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get userName() {
        return this.name ;
    }
    set userName(value) {
        this.name = value;
    }
    get userAge() {
        return this.age ;
    }
    set userAge(value) {
        this.age = value;
    }
    get userSalary() {
        return this.salary;
    }
    set userSalary(value) {
        this.salary = value;
    }

}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get userSalary() {
        return this.salary * 3;
    }

}
const Lera = new Programmer( "Lera", 15, 1000, "JS");
const Igor = new Programmer( "Igor", 38, 5000, "react");
console.log(Lera);
console.log(Igor);
