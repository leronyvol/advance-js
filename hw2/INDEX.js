const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];



const listTemplate = books.reduce((acc, el, index) => {
    if (index === 0) acc += '<ul>'
    if(!el.author){
        try {
            if (!el.author) {
                throw new Error();
            }
        } catch (e) {
            console.error(`OBJECT NUMBER ${index} DO NOT HAVE AUTOR KEY.`);
        }
    }
    if(!el.name){
        try {
            if (!el.name) {
                throw new Error();
            }
        } catch (e) {
            console.error(`OBJECT NUMBER ${index} DO NOT HAVE NAME KEY.`);
        }
    }
    if(!el.price){
        try {
            if (!el.price) {
                throw new Error();
            }
        } catch (e) {
            console.error(`OBJECT NUMBER ${index} DO NOT HAVE PRICE KEY.`);
        }
    }
    if(el.author && el.name && el.price){
        acc += `<li> author: ${el.author}</li>`
        acc += `<li>name: ${el.name}</li>`
        acc += `<li>price: ${el.price}</li>`
    }
    if (index === books.length - 1) acc += '</ul>'

    return acc
}, '')
document.querySelector('#root').innerHTML = listTemplate
