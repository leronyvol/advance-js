const usersUrl = `https://ajax.test-danit.com/api/json/users`
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';

class Card {
    constructor(user, post) {
        this.user = user;
        this.post = post;
        this.root = document.querySelector('.root')
        this.container = document.createElement('div');
        this.removeButton = document.createElement("button");
        this.handleDelete = this.handleDelete.bind(this)
    }

    handleDelete(e) { e.target.closest('.container').remove() };

    render() {
        this.removeButton.addEventListener('click', this.handleDelete);
        this.container.className = 'container'
        this.removeButton.className = 'remove-btn'
        this.removeButton.innerText = 'X'
        this.container.append(this.removeButton)
        const layout = `
                <div class="user-info">
                     <p class="card-name">${this.user.name}</p>
                     <p class="card-email">${this.user.email}</p>
                </div>
                <p class="card-title">${this.post.title}</p>
                <h2 class="card-text">${this.post.body}</h2>
        `;
        this.container.insertAdjacentHTML('beforeend', `${layout}`)
        this.root.append(this.container);
    }
}

(async () => {
    const users = await axios.get(usersUrl).then(({ data }) => data);
    const posts = await axios.get(postsUrl).then(({ data }) => data);
    posts.forEach(post => {
        const user = users.filter(user => user.id === post.userId)
        const card = new Card(...user, post)
        card.render();
    })
})();
